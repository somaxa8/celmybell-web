import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
    outDir: 'public',
    publicDir: 'static',
    site: 'https://somaxa8.gitlab.io',
    base: '/celmybell-web',
});
